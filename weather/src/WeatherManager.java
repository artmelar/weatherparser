import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;


public class WeatherManager {
    public static void main(String[] args) {
        WeatherManager weatherManager = new WeatherManager();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите путь к файлу");
        String path = scanner.nextLine();
        File fileWeather = new File(path);
        weatherManager.parse(fileWeather);
    }

    private void parse(File file) {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);

            File result = new File("result.txt");
            FileWriter fw = new FileWriter(result);
            BufferedWriter bw = new BufferedWriter(fw);

            for (int i = 0; i < 10; i++) {
                br.readLine();
            }

            String line;
            while ((line = br.readLine()) != null) {
                arrayList.add(line);
            }

            fr.close();
            br.close();

            ArrayList<Weather> weathers = new ArrayList<>();
            for (String s : arrayList) {

                int day = Integer.parseInt(s.substring(6, 8));
                int hour = Integer.parseInt(s.substring(9, 11));
                s = s.substring(14);

                float temperature = Float.parseFloat(s.substring(0, s.indexOf(',')));
                s = s.substring(s.indexOf(',') + 1);
                float humidity = Float.parseFloat(s.substring(0, s.indexOf(',')));
                s = s.substring(s.indexOf(',') + 1);
                float windSpeed = Float.parseFloat(s.substring(0, s.indexOf(',')));
                s = s.substring(s.indexOf(',') + 1);
                float windDirection = Float.parseFloat(s);

                weathers.add(new Weather(day, hour, temperature,
                        humidity, windSpeed, windDirection));
            }

            bw.write("Средняя температура воздуха: " + averageTemperature(weathers) + " °C\n" + "Средняя влажность: "
                    + averageHumidity(weathers) + " %\n" + "Средняя скорость ветра: " + averageWindSpeed(weathers) +
                    " км/ч\n" + "Самая высокая температура воздуха: " + theHighestTemperature(weathers) +
                    "Самая низкая влажность: " + theLowestHumidity(weathers) + " %\n" + "Самая высокая " +
                    "скорость ветра: " + theHighestWindSpeed(weathers) + " км/ч\n" + "Самое частое направление ветра: "
                    + theMostCommonDirection(weathers));
            bw.close();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String theHighestTemperature(ArrayList<Weather> weathers) {
        int indexTemperature = 0;
        float max = weathers.get(0).getTemperature();
        for (int i = 1; i < weathers.size(); i++) {
            if (weathers.get(i).getTemperature() > max) {
                max = weathers.get(i).getTemperature();
                indexTemperature = i;
            }
        }
        return max + " °C, дата: " + weathers.get(indexTemperature).getHour()
                + " часа(ов) " + weathers.get(indexTemperature).getDay() + " число \n";
    }

    private String theMostCommonDirection(ArrayList<Weather> weathers) {
        int[] side = new int[8];
        for (Weather weather : weathers) {
            if(weather.windDirection<22.5 && weather.windDirection>=337.5){
                side[0]++;
            }else if(weather.windDirection>=22.5 && weather.windDirection<67.5){
                side[1]++;
            }else if(weather.windDirection>=67.5 && weather.windDirection<112.5){
                side[2]++;
            }else if(weather.windDirection>=112.5 && weather.windDirection<157.5){
                side[3]++;
            }else if(weather.windDirection>=157.5 && weather.windDirection<202.5){
                side[4]++;
            }else if(weather.windDirection>=202.5 && weather.windDirection<247.5){
                side[5]++;
            }else if(weather.windDirection>=247.5 && weather.windDirection<292.5){
                side[6]++;
            }else if(weather.windDirection>=292.5 && weather.windDirection<337.5){
                side[7]++;
            }
        }

        int max = side[0];
        int indexOfMostCommonDirection = 0;
        for (int i = 1; i < 7; i++) {
            if (side[i] > max) {
                max = side[i];
                indexOfMostCommonDirection = i;
            }
        }
        String answer = "";
        switch (indexOfMostCommonDirection) {
            case (0):
                answer = "Север";
                break;
            case (1):
                answer = "Северо-восток";
                break;
            case (2):
                answer = "Восток";
                break;
            case (3):
                answer = "Юго-восток";
                break;
            case (4):
                answer = "Юг";
                break;
            case (5):
                answer = "Юго-запад";
                break;
            case (6):
                answer = "Запад";
                break;
            case (7):
                answer = "Северо-запад";
                break;
        }
        return answer;
    }

    private float theHighestWindSpeed(ArrayList<Weather> weathers) {
        float answer = weathers.get(0).windSpeed;
        for (int i = 1; i < weathers.size(); i++) {
            if (weathers.get(i).windSpeed > answer) {
                answer = weathers.get(i).windSpeed;
            }
        }
        return answer;
    }

    private float theLowestHumidity(ArrayList<Weather> weathers) {
        float answer = weathers.get(0).humidity;
        for (int i = 1; i < weathers.size(); i++) {
            if (weathers.get(i).humidity < answer) {
                answer = weathers.get(i).humidity;
            }
        }
        return answer;
    }

    private float averageWindSpeed(ArrayList<Weather> weathers) {
        float sumSpeed = 0;
        for (Weather weather : weathers) {
            sumSpeed += weather.windSpeed;
        }
        return sumSpeed / weathers.size();
    }

    private float averageHumidity(ArrayList<Weather> weathers) {
        float sumHumidity = 0;
        for (Weather weather : weathers) {
            sumHumidity += weather.humidity;
        }
        return sumHumidity / weathers.size();
    }

    private float averageTemperature(ArrayList<Weather> weathers) {
        float sumTemperature = 0;
        for (Weather weather : weathers) {
            sumTemperature += weather.temperature;
        }
        return sumTemperature / weathers.size();
    }
    //C:\Users\emaly\Downloads\Telegram Desktop\weather\dataexport_20210320T064822.csv
}


