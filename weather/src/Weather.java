public class Weather {

    int day;
    int hour;
    float temperature;
    float humidity;
    float windSpeed;
    float windDirection;

    public Weather(int day, int hour, float temperature, float humidity, float windSpeed, float windDirection) {
        this.day = day;
        this.hour = hour;
        this.temperature = temperature;
        this.humidity = humidity;
        this.windSpeed = windSpeed;
        this.windDirection = windDirection;
    }

    public int getDay() {
        return day;
    }

    public int getHour() {
        return hour;
    }

    public float getTemperature() {
        return temperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getWindSpeed() {
        return windSpeed;
    }

    public float getWindDirection() {
        return windDirection;
    }
}
